<?xml version="1.0" encoding="Windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl" xmlns:HTML="http://www.w3.org/Profiles/XHTML-transitional">
  <xsl:template><xsl:apply-templates/></xsl:template>
  <xsl:template match="text()"><xsl:value-of/></xsl:template>
  
  <xsl:template match="/">
    <HTML>
      <HEAD>
        <TITLE><xsl:value-of select="fabula/tale/headline"/></TITLE>
        <STYLE>
          BODY       { margin:0px; background-color: #FFFFDD; width: 30em;
                       font-family: Arial, Helvetica, sans-serif; font-size: small; }
          H1         { color: #888833; }
          P          { margin-top: .5em; margin-bottom: .25em; }
          HR         { color: #888833; }
          .biblio   { text-align: right; font-size: xx-small; margin-top: .25em; }
         
          .body      { text-align: justify; background-color: #FFFFDD; }
         
          .person    { font-weight: bold; }
          .number      { font-weight: bold; }
        
        </STYLE>
        <SCRIPT language="JavaScript"><xsl:comment><![CDATA[
          function show() {
            full.style.display="block";
            addmotif.style.display="none";
          }

          function hide() {
            addmotif.style.display="block";
            full.style.display="none";
          }

         
        ]]></xsl:comment></SCRIPT>
      </HEAD>

      <BODY>
	 
        <TABLE WIDTH="80%" CELLSPACING="8">
          <TR>
            <TD colspan="2">
              
            </TD>
          </TR>
          <TR>
            <TD WIDTH="20%" VALIGN="top" STYLE="padding-top:2em">
              <P class="biblio">
              
               
              </P>
         
              <xsl:for-each select="fabula/tale">
                <P class="biblio"><xsl:value-of select="biblio/publisher"/></P>
                <P class="biblio"><xsl:value-of select="biblio/number"/>, <xsl:value-of select="biblio/thompson"/></P>
                <P class="biblio"><xsl:value-of select="archiv"/></P>
				<P class="tagline">
                  <A href="javascript:hide();">������ ������</A>
                </P> <P class="tagline">
                  <A href="javascript:show();">������ �������</A>
                </P>
              </xsl:for-each>
            </TD>
            <TD class="body">
              <P class="tagline"><xsl:value-of select="fabula/review/date"/></P>
              <DIV id="addmotif"  STYLE="display:none">
                <P><div class="onlymotifs"><b><xsl:value-of select="fabula/mainmotif//addmotif/@name"/>: </b></div><xsl:value-of select="fabula/mainmotif//addmotif"/> </P>
               
              </DIV>
              <DIV id="full" STYLE="display:block">
                <xsl:apply-templates select="fabula/mainmotif"/>
                
                
              </DIV>
              
              
            </TD>
          </TR>
        </TABLE>
        
        <P/>
      </BODY>
    </HTML>
  </xsl:template>
<xsl:template match="mainmotif">
    <P><b><xsl:value-of select="@number"/></b> <xsl:apply-templates/></P>
  </xsl:template>

  <xsl:template match="function">
    <P><b><xsl:value-of select="@name"/></b> <xsl:apply-templates/></P>
  </xsl:template>

  <xsl:template match="person">
    <SPAN class="person"><xsl:apply-templates/></SPAN>
  </xsl:template>

  <xsl:template match="number">
    <SPAN class="number"><xsl:apply-templates/></SPAN>
  </xsl:template>

  <xsl:template match="self">
    <SPAN class="self"><xsl:apply-templates/></SPAN>
  </xsl:template>

 
 

  

  
</xsl:stylesheet>