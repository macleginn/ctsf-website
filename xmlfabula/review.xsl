<?xml version="1.0" encoding="Windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl" xmlns:HTML="http://www.w3.org/Profiles/XHTML-transitional">
  <xsl:template><xsl:apply-templates/></xsl:template>
  <xsl:template match="text()"><xsl:value-of/></xsl:template>
  
  <xsl:template match="/">
    <HTML>
     

      <BODY><STYLE>
          BODY       { margin:0px; width: 30em;
                       font-family: Arial; font-size: smaller; }
          H1         { color: #888833; }
          P          { margin-top: .5em; margin-bottom: .25em;  font-family: Arial; font-size: smaller;}
          HR         { color: #888833; }
          .biblio   { text-align: right; font-size: xx-small; margin-top: .25em; }
         
          .body      { text-align: justify; background-color: #FFFFDD; }
         
          .person    { font-weight: bold; }
          .number      { font-weight: bold; }
          .motif     {color:blue;}
        </STYLE>
        
        
              <H4 align="center">
               
                <xsl:value-of select="fabula/mainmotif/@number"/> <xsl:value-of select="fabula/mainmotif/@name"/> 
              </H4>
<table cellpadding="20"><tr><td>            
            
         
              <xsl:for-each select="fabula/tale">
                <P class="biblio"><xsl:value-of select="biblio/publisher"/></P>
                <P class="biblio"><xsl:value-of select="biblio/number"/>, <xsl:value-of select="biblio/thompson"/></P>
                <P class="biblio"><xsl:value-of select="archiv"/></P>
				
              </xsl:for-each>
           
         
              <P class="tagline"><xsl:value-of select="fabula/review/date"/></P>
              
              <DIV id="full" STYLE="display:block"> 
                <xsl:apply-templates select="fabula/mainmotif"/>
                
                
              </DIV>
              
              
        
        
        <P/></td></tr></table>
      </BODY>
    </HTML>
  </xsl:template>
  
  <xsl:template match="function">
  
    <P><b><xsl:value-of select="@name"/></b><br/> <xsl:apply-templates/></P>
  </xsl:template>
<xsl:template match="addmotif">
<b class="motif"><xsl:value-of select="@number"/>: <xsl:value-of select="@name"/> </b><i class="motif"><xsl:apply-templates/></i>
    
  </xsl:template>
  <xsl:template match="person">
    <SPAN class="person"><xsl:apply-templates/></SPAN>
  </xsl:template>

  <xsl:template match="number">
    <SPAN class="number"><xsl:apply-templates/></SPAN>
  </xsl:template>

  <xsl:template match="self">
    <SPAN class="self"><xsl:apply-templates/></SPAN>
  </xsl:template>

 
 

  

  
</xsl:stylesheet>