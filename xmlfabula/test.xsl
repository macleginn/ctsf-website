<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl" >
<xsl:template match="/" >
<html>
<head/> 
<body>
<h1 style="font-family: 'Century Schoolbook', 'Book Antigua', 'Times New Roman', Times, serif; font-weight: bold; font-size: 2em; color: brown; background-color: transparent; margin-left: 6%; margin-right: 6%; border-bottom: solid thin brown; position: relative;" > Supportive Feedback</h1> 
<h2 style="font-family: 'Century Schoolbook', 'Book Antigua', 'Times New Roman', Times, serif; font-weight: bold; font-size: 1.3em; margin-left: 6%; margin-top: 1.75em; margin-bottom: .2em; color: brown; background-color: transparent;" > from Commercial Publications:</h2> 
<xsl:apply-templates select="//review[@type='responsible']/revitem" /> 
<h2 style="font-family: 'Century Schoolbook', 'Book Antigua', 'Times New Roman', Times, serif; font-weight: bold; font-size: 1.3em; margin-left: 6%; margin-top: 1.75em; margin-bottom: .2em; color: brown; background-color: transparent;" > from International Readers:</h2> 
<xsl:apply-templates select="//review[@type='international']/revitem" /> 
<h2 style="font-family: 'Century Schoolbook', 'Book Antigua', 'Times New Roman', Times, serif; font-weight: bold; font-size: 1.3em; margin-left: 6%; margin-top: 1.75em; margin-bottom: .2em; color: brown; background-color: transparent;" > from selection of Easily Amused Readers:</h2> 
<xsl:apply-templates select="//review[@type='amused']/revitem" /> 
</body> 
</html> 
</xsl:template> 
<xsl:template match="//review[@type='responsible']/revitem" >
<div style="margin-left: 6%; margin-right: 6%; margin-bottom: 1.5em; background-color: cornsilk; padding-bottom: .6em; padding-top: .6em;" >
<xsl:value-of select="text()" /> 
<xsl:apply-templates/> 
</div> 
</xsl:template> 
<xsl:template match="//review[@type='international']/revitem" >
<div style="margin-left: 6%; margin-right: 6%; margin-bottom: 1.5em; background-color: cornsilk; padding-bottom: .6em; padding-top: .6em;" >
<xsl:value-of select="text()" /> 
<xsl:apply-templates/> 
</div> 
</xsl:template> 
<xsl:template match="//review[@type='amused']/revitem" >
<div style="margin-left: 6%; margin-right: 6%; margin-bottom: 1.5em; background-color: cornsilk; padding-bottom: .6em; padding-top: .6em;" >
<xsl:value-of select="text()" /> 
<xsl:apply-templates/> 
</div> 
</xsl:template> 
<xsl:template match="para" >
<p style="display: block; font-weight: bold; font-size: .8em; color: black; background-color: transparent; margin-top: .5em; margin-bottom: .1em; margin-left: 6%; margin-right: 6%; position: relative;" >
<xsl:apply-templates/> 
</p> 
</xsl:template> 
<xsl:template match="br" >
<br/> 
</xsl:template> 
<xsl:template match="i" >
<i>
<xsl:apply-templates/> 
</i> 
</xsl:template> 
<xsl:template match="emph" >
<b style="font-weight: bold; color: brown;" >
<xsl:apply-templates/> 
</b> 
</xsl:template> 
<xsl:template match="text()" >
<xsl:value-of/> 
</xsl:template> 
</xsl:stylesheet> 