keyword_set = {};
keyword_dic = compileSearchDict(records);
object_dic  = compileObjectDict(records);
fulltext_d  = compileFullTextDict(records);

function normaliseWord(word) {
	var temp = word.toLowerCase().split("");
	var newWord = [];
	var re = /[�-�\-�]/;
	for (var i = 0; i < temp.length; i++) {
		if (re.test(temp[i])) {
			newWord.push(temp[i])
		}
	}
	return newWord.join('')
}

function compileFullTextDict(records) {
	console.log("Compiling full-text-search dict");
	var ftd = {};
	for (var i = 0; i < records.length; i++) {
		if (records[i]["��� ������"] != "�����") {
			continue;
		}
		// console.log(i);
		var localSearchObj = {};
		var words = records[i]["��������� ������"].split(" ");
		// console.log(words);
		for (var j = 0; j < words.length; j++) {
			var key = normaliseWord(words[j]);
			if (key.length > 0) {
				localSearchObj[key] = true;
			}
		}
		ftd[i] = localSearchObj;
	}
	console.log("Done");
	return ftd;
}

function initialisePage() {
	var b = $('body');
	b.append($("<label>").attr("class", "option_label").attr("for", "objects").html("�������� ��������: "));
	var obj_list = $("<select>").attr("id", "objects").attr("class", "search_param").attr("onchange", "printRecords(this)");
	obj_list.append($("<option>").attr("value", "�").html("�"));
	b.append(obj_list);
	b.append($("<br/>"));
	b.append($("<label>").attr("class", "option_label").attr("for", "keywords").html("�������� �����: ").attr("id", "keywords_label").attr("onmouseover", "showKeyWords(event)").attr("onmouseout", "hideKeyWords()"));
	b.append($("<input>").attr("type", "text").attr("class", "search_param").attr("id", "keywords"));
	b.append($("<input>").attr("type", "button").attr("value", "������").attr("onclick", "queryKeywords()"));
	b.append($("<br/>"));
	b.append($("<label>").attr("class", "option_label").attr("for", "text_search").html("�������������� �����: "));
	b.append($("<input>").attr("type", "text").attr("class", "search_param").attr("id", "text_search"));
	b.append($("<input>").attr("type", "button").attr("value", "������").attr("onclick", "queryFullText()"));
	b.append($("<div>").attr("id", "report_area"));
	var objects_keys = get_els(objects);
	objects_keys.sort();
	for (var i = 0; i < objects_keys.length; i++) {
		var new_opt = $("<option>").attr("value", objects_keys[i]).text(objects[objects_keys[i]]["���������� ������� �����"] + ", " + objects[objects_keys[i]]["���������� �����"] + ", " + objects[objects_keys[i]]["������"]);
		$('#objects').append(new_opt);
	}
	var keyword_div = $("<div>").attr("id", "keyword_div").html('<p class="keyword_list"><b>��� ������� �����:</b> ����, �������, �����, �������, ����������, ����������� ���, ��������, ��������, ��������� �����, ��������� �����, �������� �����, ��������, �����, ����, ������, ������, ����, ������, ����, ������, ������, ��������, �����������, �������</p><p class="keyword_list"><b>��������� ������� �����:</b> ������, ��������, �����, �������, ���������, �������, �������, ����������� ��������, �����������, ���������, �������� ���, �������������, �������� �������, ����, ����������</p><p class="keyword_list"><b>���������� ���������:</b> �����, ����, �����</p><p class="keyword_list"><b>���������� �������� (������):</b> ������, ������, ���������������, �������������, ��������������, ����������, �����������, ������, ���������, �������, ����������, ����������</p><p class="keyword_list"><b>���������� �������� (����� �����������):</b> ���������, �������, ������, �������, �����, ����������, �����������</p><p class="keyword_list"><b>���������� �������� (������ �����������):</b> �������, ��������, �����, ��������, �������, ������, ��������, �������</p><p class="keyword_list"><b>���������� �������� (���� � ������):</b> ��������, ����, �����, ��������, ����������, ��������, ��������</p><p class="keyword_list"><b>�������� �������:</b> �����, ������, �������, �������� �����, ������ �����, ������� ������, ����� �������, ����� ������, ��������� �����, ���, �����������, ���������, �����, ������� ������������</p><p class="keyword_list"><b>���������������� ��������:</b> �������������, ���������, ������� ������������ ���������������� �������, ������������, ������� ������������������ �������, ����������, ������������, �������, ������������������ ��������� �������, �������, ��������, ������, ���������, ����������, ����������, �������, ������, �����������, �������, ���������, �������, ����������, �������� �� ��������</p>');
	b.append(keyword_div);
}

function queryFullText() {
	var query = $("#text_search").val().trim();
	if (query == "" || query == "undefined") {
		return
	}
	query = query.split(' ');
	for (var i = 0; i < query.length; i++) {
		query[i] = normaliseWord(query[i]);
	}
	var results = [];
	for (var key in fulltext_d) {
		var searchHit = true;
		for (var i = 0; i < query.length; i++) {
			if (!fulltext_d[key].hasOwnProperty(query[i])) {
				searchHit = false;
				break;
			}
		}
		if (searchHit) {
			results.push(records[key]);
		}
	}
	$("div#report_area").empty();
	if (results.length == 0) {
		$("div#report_area").append("<p>������� � ������ ����������� ���� ���.");
		return
	}
	for (var i = 0; i < results.length; i++) {
		var record_div = convertRecordToHTML(results[i], query);
		// TODO: �������� ��������� ������ ��������� ����.
		$("div#report_area").append(record_div);
	}
}

function showKeyWords(e) {
	// alert(e.clientX);
	var left = (e.clientX + 10) + "px";
    var top  = (e.clientY + 10) + "px";
    var div = document.getElementById("keyword_div");
    div.style.left = left;
    div.style.top = top;
    $("#keyword_div").toggle();
    return false;
}

function hideKeyWords() {
	$("#keyword_div").toggle();
}

function queryKeywords() {
	var query = $("#keywords").val();
	if (query == "") {
		return
	}
	query = query.split(/\s*,\s*/);
	for (var i = 0; i < query.length; i++) {
		if (!hasItem(keyword_set,query[i].toLowerCase())) {
			alert('�' + query[i] + "� ����������� � ������ �������� ����!")
			return
		}
	}
	var records_set = keyword_dic[query[0].toLowerCase()]
	if (query.length > 1) {
		for (var i = 1; i < query.length; i++) {
			records_set = intersection(records_set, keyword_dic[query[i].toLowerCase()]);
		}
	}
	var for_output = get_els(records_set);
	$("div#report_area").empty();
	if (for_output.length == 0) {
		$("div#report_area").append("<p>������� � ������ ����������� �������� ���� ���.");
		return
	}
	for_output.sort();
	for (var i = 0; i < for_output.length; i++) {
		var record_div = convertRecordToHTML(records[for_output[i]], query);
		$("div#report_area").append(record_div);
	}
}

function printRecords(argument) {
	$("div#report_area").empty();
	var obj_key = argument.value;
	var div = convertObjectToHTML(obj_key, objects[obj_key]);
	// console.log(div.text());
	$("div#report_area").append(div);
}

function convertObjectToHTML(obj_key, record_obj) {
	// console.log(record_obj);
	var div = $("<div>").attr("class", "object_report");
	div.append("<h3>�������� �������:</h3>");
	var fields = get_els(record_obj);
	fields.sort();
	for (var i = 0; i < fields.length; i++) {
		var f = fields[i];
		if (typeof(record_obj[f]) === "string" && record_obj[f].length > 0) {
			var p = $("<p>").html("<b>" + f + ":</b> " + record_obj[f]);
			div.append(p);
		} else if (record_obj[f].constructor === Array && record_obj[f].length > 0) {
			var p = $("<p>").html("<b>" + f + ":</b> " + record_obj[f].join(", "));
			div.append(p);
		}
	}
	// �������� ��������� ������� �� �����.
	div.append("<h3>������, ����������� � �������:</h3>");
	var pertinent_records = object_dic[obj_key];
	// console.log(pertinent_records);
	for (var i = 0; i < pertinent_records.length; i++) {
		var r_div = convertRecordToHTML(records[pertinent_records[i]]);
		// console.log(r_div.text());
		div.append(r_div);
	}
	return div
}

function convertRecordToHTML(r, query) {
	var r_div = $("<div>").attr("class", "record");
	var fields = get_els(r);
	// console.log(fields);
	fields.sort();
	for (var i = 0; i < fields.length; i++) {
		var f = fields[i];
		if (f === "������") {
			continue;
		}
		// �������� ������� ����� ��� �������� �����.
		if (typeof(r[f]) === "string" && r[f].length > 1) {
			var p = $("<p>").html("<b>" + f + ":</b> ");
			if (f === "��������� ������" && query != undefined) {
				var text4Processing = r[f].split(' ');
				var newText = [];
				for (var j = 0; j < text4Processing.length; j++) {
					var queryWord = false
					for (var k = 0; k < query.length; k++) {
						if (normaliseWord(text4Processing[j]) == query[k]) {
							newText.push('<span class="searchHit">' + text4Processing[j] + "</span>");
							queryWord = true;
							break;
						} else if (text4Processing[j].substring(0, 2) === "ht") {
							newText.push('<a target="_blank" href="' + text4Processing[j] + '">' + text4Processing[j] + '</a>');
							queryWord = true;
							break;
						}
					}
					if (!queryWord) {
						newText.push(text4Processing[j]);
					}
				}
				p.append(newText.join(" "));
			} else {
				var text4Processing = r[f].trim().split(' ');
				var newText = [];
				for (var j = 0; j < text4Processing.length; j++) {
					if (text4Processing[j].substring(0, 2) === "ht") {
						newText.push('<a target="_blank" href="' + text4Processing[j] + '">' + text4Processing[j] + '</a>');
					} else {
						newText.push(text4Processing[j]);
					}
				}
				p.append(newText.join(" "));
			}
			r_div.append(p);
		} else if (r[f].constructor === Array && r[f].length > 1) {
			// �������� �������������� ����������� � ���������� ������.
			var p = $("<p>").html("<b>" + f + ":</b> " + r[f].join(", "));
			r_div.append(p);
		}
	}
	return r_div;
}

function get_els(set) {
	result = [];
	for (var property in set) {
		if (set.hasOwnProperty(property)) {
			result.push(property)
		}
	}
	return result
}

function addToSet(set, item) {
	set[item] = true;
}

function addToSetFromList(set, list) {
	for (var i = 0; i < list.length; i++) {
		addToSet(set, list[i]);
	}
}

function hasItem(set, item) {
	return set.hasOwnProperty(item)
}

function intersection(set1, set2) {
	var props = get_els(set1);
	var copy = {};
	addToSetFromList(copy, props);
	for (var i = 0; i < props.length; i++) {
		if (!hasItem(set2, props[i])) {
			delete copy[props[i]]
		}
	}
	return copy
}

function compileObjectDict(records) {
	console.log("Compiling object dict");
	var obj_dic = {};
	for (var i = 0; i < records.length; i++) {
		if (!hasItem(obj_dic, records[i]["������"])) {
			obj_dic[records[i]["������"]] = [];
		}
		obj_dic[records[i]["������"]].push(i);
	}
	return obj_dic;
}

function compileSearchDict(records) {
	console.log("Compiling search dict");
	var search_dic = {};
	for (var i = 0; i < records.length; i++) {
		var fields = get_els(records[i]);
		for (var j = 0; j < fields.length; j++) {
			var field = fields[j];
			if (records[i][field].constructor === Array && records[i][field].length > 0) {
				keywords = records[i][field]
				for (var k = 0; k < keywords.length; k++) {
					var kword = keywords[k].toLowerCase()
					addToSet(keyword_set, kword);
					if (!hasItem(search_dic, kword)) {
						search_dic[kword] = {};
					}
					addToSet(search_dic[kword], i);
				}
			}
		}
	}
	return search_dic
}
