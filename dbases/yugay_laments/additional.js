function popup(text_key) {
		if (!window.focus) {
			return true;
		}
		var infoWindow = window.open('', '�������� ������', 'width=400,height=200,scrollbars=yes');
		var wdoc = infoWindow.document;
		wdoc.write('<p>��������: ' + textCollection[text_key]['source'] + '</p>');
		if (textCollection[text_key]['collector'] != '' && textCollection[text_key]['collector'] != null) {
			wdoc.write('<p>����������: ' + textCollection[text_key]['collector'] + '</p>');
		}
		if (textCollection[text_key]['performer'] != '' && textCollection[text_key]['performer'] != null) {
			wdoc.write('<p>�����������: ' + textCollection[text_key]['performer'] + '</p>');
		}
		if (textCollection[text_key]['year'] != '' && textCollection[text_key]['year'] != null) {
			wdoc.write('<p>��� ������: ' + textCollection[text_key]['year'] + '</p>');
		}
		if (textCollection[text_key]['place_of_recording'] != '' && textCollection[text_key]['place_of_recording'] != null) {
			wdoc.write('<p>����� ������: ' + textCollection[text_key]['place_of_recording'] + '</p>');
		}
		if (textCollection[text_key]['coords_of_recording'] != '' && textCollection[text_key]['coords_of_recording'] != null) {
			wdoc.write('<p>���������� ����� ������: ' + textCollection[text_key]['coords_of_recording'].join(', ') + '</p>');
		}
		if (textCollection[text_key]['place_of_birth'] != '' && textCollection[text_key]['place_of_birth'] != null) {
			wdoc.write('<p>����� �������� �����������: ' + textCollection[text_key]['place_of_birth'] + '</p>');
		}
		if (textCollection[text_key]['coords_of_birthplace'] != '' && textCollection[text_key]['coords_of_birthplace'] != null) {
			wdoc.write('<p>���������� ����� �������� �����������: ' + textCollection[text_key]['coords_of_birthplace'].join(', ') + '</p>');
		}
		if (textCollection[text_key]['date_of_birth'] != '' && textCollection[text_key]['date_of_birth'] != null) {
			wdoc.write('<p>��� �������� �����������: ' + textCollection[text_key]['date_of_birth'] + '</p>');
		}
		return false;
	}
	var popupVar = popup;

var A_dict = {};
var B_dict = {};
var C_dict = {};
var all_ids = {};
var id_to_value = {};
var A_kwords = [],
	B_kwords = [],
	C_kwords = [];

String.prototype.titleize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

function compileSearchDicts() {
	for (var fragment_id in fragmentCollection) {
		if (!fragmentCollection.hasOwnProperty(fragment_id)) {
			continue;
		}
		all_ids[fragment_id] = true;
		for (var i = 0; i < fragmentCollection[fragment_id]['A_group'].length; i++) {
			var tag = fragmentCollection[fragment_id]['A_group'][i];
			if (!A_dict.hasOwnProperty(tag)) {
				A_dict[tag] = {};
			}
			A_dict[tag][fragment_id] = true;
		}
		for (var i = 0; i < fragmentCollection[fragment_id]['B_group'].length; i++) {
			var tag = fragmentCollection[fragment_id]['B_group'][i];
			if (!B_dict.hasOwnProperty(tag)) {
				B_dict[tag] = {};
			}
			B_dict[tag][fragment_id] = true;
		}
		for (var i = 0; i < fragmentCollection[fragment_id]['C_group'].length; i++) {
			var tag = fragmentCollection[fragment_id]['C_group'][i];
			if (!C_dict.hasOwnProperty(tag)) {
				C_dict[tag] = {};
			}
			C_dict[tag][fragment_id] = true;
		}
	}
	var id_counter = 0;
	var A_keys = [];
	$.each(A_dict, function(key, value) {
		A_keys.push(key);
	});
	A_keys.push('�������� (�����)');
	A_keys.push('������ (�����)');
	A_keys.sort();

	for (var i = 0; i < A_keys.length; i++) {
		var currentID = "";
		if (A_keys[i] == '������') {
			currentID = "check" + id_counter;
			id_to_value[currentID] = "������";
			id_counter++;
			$("#checkA").append(
				$("<input></input>")
				.attr("type", "checkbox")
				.attr("id", currentID)
				.attr("onclick", "repopulateMenu(this)")
				.attr("value", "������")
				)
			$("#checkA").append(
				$("<label></label>")
				.attr("for", currentID)
				.text("  " + "������ (��������������)".titleize())
				)
			$("#checkA").append($("<br/>"));
		} else {
			currentID = "check" + id_counter;
			id_to_value[currentID] = A_keys[i];
			id_counter++;
			$("#checkA").append(
				$("<input></input>")
				.attr("type", "checkbox")
				.attr("id", currentID)
				.attr("onclick", "repopulateMenu(this)")
				.attr("value", A_keys[i])
				)
			$("#checkA").append(
				$("<label></label>")
				.attr("for", currentID)
				.text("  " + A_keys[i].titleize())
				)
			$("#checkA").append($("<br/>"));
		}
	}

	var B_keys = [];
	$.each(B_dict, function(key, value) {
		if (key != '���') {
			var key_stats = [0, 0];
			var texts_for_this_key = {};
			for (var frag in B_dict[key]) {
				if (B_dict[key].hasOwnProperty(frag)) {
					key_stats[0]++;
				}
				texts_for_this_key[fragmentCollection[frag]['text_id']] = true;
			}
			for (var text in texts_for_this_key) {
				if (texts_for_this_key.hasOwnProperty(text)) {
					key_stats[1]++;
				}
			}
			B_keys.push([key, key + ': ' + key_stats[0] + ' (' + key_stats[1] + ')']);
		}
	});

	B_keys.sort();

	for (var i = 0; i < B_keys.length; i++) {
		currentID = "check" + id_counter;
		id_counter++;
		id_to_value[currentID] = B_keys[i];
		$("#checkB").append(
			$("<input></input>")
			.attr("type", "checkbox")
			.attr("id", currentID)
			.attr("onclick", "repopulateMenu(this)")
			.attr("value", B_keys[i][0])
			)
		$("#checkB").append(
			$("<label></label>")
			.attr("for", currentID)
			.text("  " + B_keys[i][1].titleize())
			)
		$("#checkB").append($("<br/>"));
	}

	$.each(C_dict, function(key, value) {
		if (key != "") {
			currentID = "check" + id_counter;
			id_counter++;
			id_to_value[currentID] = key;
			$("#checkC").append(
				$("<input></input>")
				.attr("type", "checkbox")
				.attr("id", currentID)
				.attr("onclick", "repopulateMenu(this)")
				.attr("value", key)
				)
			$("#checkC").append(
				$("<label></label>")
				.attr("for", currentID)
				.text("  " + key.titleize())
				)
			$("#checkC").append($("<br/>"));
		}
	});
};

function compareNumbersAsStrings(a, b) {
	var prefix1 = a.split('_')[0];
	var suffix1 = parseInt(a.split('_')[1]);
	var prefix2 = b.split('_')[0];
	var suffix2 = parseInt(b.split('_')[1]);
	if (prefix1 > prefix2) {
		return 1;
	} else if (prefix2 > prefix1) {
		return -1;
	} else {
		return suffix1 - suffix2;
	}
};

function addFragment(destination, fragment_id) {
	var fragment_obj = fragmentCollection[fragment_id];
	var endpoints = fragment_obj['lines'].split('�');
	var text_id = fragment_obj['text_id'];
	endpoints[0] = parseInt(endpoints[0])
	if (endpoints.length < 2) {
		endpoints[1] = endpoints[0] + 1;
	} else {
		endpoints[1] = parseInt(endpoints[1]) + 1;
	}
	var new_div = $('<div></div>').attr('class', 'result_div');
	var text_section = $('<p></p>').attr('class', 'fragment_text');
	var first_line = $('<p></p>');
	first_line.append(
		$('<a></a>')
		.attr('onClick', 'return popup(\'' + text_id + '\')')
		.attr('class', 'text_info')
		.attr('href', '#')
		.text(fragment_obj['text_id'])
		);
	first_line.append(': ' + fragment_obj['lines']);
	text_section.append(first_line);
	text_section.append(fragment_obj['fragment'].join('<br/>'));
	text_section.append('<hr/>');

	new_div.append(text_section);
	destination.append(new_div);
};

function has(a, b) {
	return a.hasOwnProperty(b)
}

function update(a, b) {
	for (var element in b) {
		if (b.hasOwnProperty(element)) {
			a[element] = true;
		}
	}
}

function isEmpty(map) {
	for (var key in map) {
		if (map.hasOwnProperty(key)) {
			return false
		}
	}
	return true
}

function nElements(map) {
	var count = 0;
	for (var key in map) {
		if (map.hasOwnProperty(key)) {
			count++;
		}
	}
	return count
}

function intersection(a, b) {
	var result = {};
	for (var element in a) {
		if (has(b, element)) {
			result[element] = true;
		}
	}
	return result;
}

function sortedKeys(map) {
	var keys = [];
	for (var key in map) {
		if (map.hasOwnProperty(key)) {
			keys.push(key);
		}
	}
	keys.sort();
	return keys
}

function makeQuery(fixedVal) {
	// Returns a list of results for disabling menu items.
	var A_frags = {},
		B_frags = {},
		C_frags = {};

	if (fixedVal == "�������� (�����)" || fixedVal == "������ (�����)") {
		var search_key = fixedVal.split(' ')[0];
		for (var a_key in A_dict) {
			if (!A_dict.hasOwnProperty(a_key)) {
				continue;
			}
			if (a_key.split(' � ')[0] == search_key) {
				update(A_frags, A_dict[a_key]);
			}
		}
	} else {
		if (has(A_dict, fixedVal)) {
			update(A_frags, A_dict[fixedVal]);
		} else if (has(B_dict, fixedVal)) {
			update(B_frags, B_dict[fixedVal]);
		} else if (has(C_dict, fixedVal)) {
			update(C_frags, C_dict[fixedVal]);
		} else {
			console.log(fixedVal);
			console.log("Value of a checkbox not found in any dict!");
		}
	}

	var a_checked = $("#checkA").children("input:checked");
	for (var i = 0; i < a_checked.length; i++) {
		var child = a_checked[i];
		if (child.value == "�������� (�����)" || child.value == "������ (�����)") {
			var search_key = child.value.split(' ')[0];
			for (var a_key in A_dict) {
				if (!A_dict.hasOwnProperty(a_key)) {
					continue;
				}
				if (a_key.split(' � ')[0] == search_key) {
					update(A_frags, A_dict[a_key]);
				}
			}
		} else {
			update(A_frags, A_dict[child.value]);
		}
	}
	if (isEmpty(A_frags)) {
		A_frags = all_ids;
	}
			
	var b_checked = $("#checkB").children("input:checked");
	b_checked.push(fixedVal);
	for (var i = 0; i < b_checked.length; i++) {
		var child = b_checked[i];
		update(B_frags, B_dict[child.value]);
	}
	if (isEmpty(B_frags)) {
		B_frags = all_ids;
	}

	var c_checked = $("#checkC").children("input:checked");
	c_checked.push(fixedVal);
	for (var i = 0; i < c_checked.length; i++) {
		var child = c_checked[i];
		update(C_frags, C_dict[child.value]);
	}
	if (isEmpty(C_frags)) {
		C_frags = all_ids;
	}

	results = [];
	for (var key in A_frags) {
		if (B_frags.hasOwnProperty(key) && C_frags.hasOwnProperty(key)) {
			results.push(key);
		}
	}
	
	return results

};

var resultList;

function makeQueryAndPublish() {
	$("#searchResults").empty();
	$("#searchResults").append(
		$("<h3>").text("���������� ������ �� ���������� �������� ����:"));
	resultList = [];
	A_kwords = [];
	B_kwords = [];
	C_kwords = [];
	var A_frags = {},
		B_frags = {},
		C_frags = {};

	var a_checked = $("#checkA").children("input:checked");
	for (var i = 0; i < a_checked.length; i++) {
		var child = a_checked[i];
		if (child.value == "�������� (�����)" || child.value == "������ (�����)") {
			A_kwords.push(child.value.split(' ')[0]);
		} else {
			A_kwords.push(child.value);
		}
	}
	var b_checked = $("#checkB").children("input:checked");
	for (var i = 0; i < b_checked.length; i++) {
		B_kwords.push(b_checked[i].value);
	}
	var c_checked = $("#checkC").children("input:checked");
	for (var i = 0; i < c_checked.length; i++) {
		C_kwords.push(c_checked[i].value);
	}
	// Creating all combinations of query words.
	var workingList = [],
		tempList = [];
	if (A_kwords.length != 0) {
		for (var i = 0; i < A_kwords.length; i++) {
			workingList.push([A_kwords[i]]);
		}
	}
	if (B_kwords.length != 0) {
		if (workingList.length == 0) {
			for (var i = 0; i < B_kwords.length; i++) {
				workingList.push([B_kwords[i]]);
			}
		} else {
			for (var i = 0; i < workingList.length; i++) {
				var incompleteSearch = workingList[i];
				for (var j = 0; j < B_kwords.length; j++) {
					tempList.push(incompleteSearch.concat(B_kwords[j]));
				}
			}
			workingList = tempList.slice(0);
			tempList = [];
		}
	}
	if (C_kwords.length != 0) {
		if (workingList.length == 0) {
			for (var i = 0; i < C_kwords.length; i++) {
				workingList.push([C_kwords[i]]);
			}
		} else {
			for (var i = 0; i < workingList.length; i++) {
				var incompleteSearch = workingList[i];
				for (var j = 0; j < C_kwords.length; j++) {
					tempList.push(incompleteSearch.concat(C_kwords[j]));
				}
			}
			workingList = tempList.slice(0);
		}
	}
	var qcn = 0;
	workingList.forEach(function(queryComb) {
		A_frags = {};
		B_frags = {};
		C_frags = {};
		queryComb.forEach(function(queryWord) {
			update(A_frags, A_dict[queryWord]);
			update(B_frags, B_dict[queryWord]);
			update(C_frags, C_dict[queryWord]);
		});
		if (isEmpty(A_frags)) {
			A_frags = all_ids;
		}
		if (isEmpty(B_frags)) {
			B_frags = all_ids;
		}
		if (isEmpty(C_frags)) {
			C_frags = all_ids;
		}
		var results = [];
		for (var key in A_frags) {
			if (B_frags.hasOwnProperty(key) && C_frags.hasOwnProperty(key)) {
				results.push(key);
			}
		}
		resultList.push(results)
		console.log(queryComb + ": " + resultList[qcn].length)
		if ((resultList[qcn].length >= 5 && resultList[qcn].length <= 20) || resultList[qcn].length == 0) {
			var s = "����������";
		} else {
			var mod = resultList[qcn].length % 10;
			if (mod == 1) {
				var s = "��������";
			} else if (mod < 5) {
				var s = "���������";
			} else {
				var s = "����������";
			}
		}
		$("#searchResults").append(
			$("<p>")
			.attr("class", "result")
			.html(
				"<a href=\"#\" onClick=\"writeResultsToPopup(" + qcn + ")\">" + queryComb.join("; ").titleize() + "</a>: " + resultList[qcn].length + " " + s)
			)
		qcn++;
	});
}

function clearForm() {
	var alist = $("#checkA").children()
	var blist = $("#checkB").children()
	var clist = $("#checkC").children()
	var alll = [alist, blist, clist];
	for (var i = 0; i < alll.length; i++) {
		var currentList = alll[i];
		currentList.each(function (el) {
			var element = $(currentList[el]);
			element.attr('checked', false);
			element.attr('disabled', false);
		});
	}
}

function writeResultsToPopup(resultNum) {
	examplesDiv = $("#exDiv");
	examplesDiv.empty();
	examplesDiv.html('<a href="#" onClick="document.getElementById(\'exDiv\').style.display=\'none\';document.getElementById(\'exBgr\').style.display=\'none\';">�������</a>')
	examplesDiv.css("display", "block");
	($("#exBgr")).css("display", 'block');
	for (var i = 0; i < resultList[resultNum].length; i++) {
		addFragment(examplesDiv, resultList[resultNum][i]);
	}
}

function repopulateMenu(callingCheckbox) {
	var callingDiv = $(callingCheckbox).parent().attr("id");
	var divList = ["checkA", "checkB", "checkC"];
	for (var j = 0; j < divList.length; j++ ) {
		var divID = divList[j];
		if (divID == callingDiv) {
			continue
		}
		var toCheck = $("#" + divID).children("input");
		for (var i = 0; i < toCheck.length; i++) {
			var results = makeQuery(toCheck[i].value);
			if (results.length == 0) {
				toCheck[i].checked = false;
				toCheck[i].disabled = true;
			} else {
				toCheck[i].disabled = false;
			}
		}
	}
}