<!DOCTYPE xsl:stylesheet
  [
    <!ENTITY nbsp   "&#160;">
    <!ENTITY mdash  "&#8212;">
  ]
>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="4.01"
       encoding="utf-8"
       indent="yes"/>

<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head></head>
<body>

<xsl:for-each select="bibliography/sort-keys/key" >
<xsl:variable name="key"><xsl:value-of select="."/></xsl:variable>

<div class='t2'><h3><xsl:copy-of select="$key"/></h3></div><br/>

<xsl:for-each select="/bibliography/entry">
	<xsl:sort select="year" />
	<xsl:choose>
	<xsl:when test='topic=$key'>

	<xsl:variable name="link">
		<xsl:choose>
			<xsl:when test="filename=''"></xsl:when>
			<xsl:otherwise>&nbsp;<a href="materials/publ_pdf/{filename}.pdf"><img src="images/pdf_icon.gif"/></a></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="isbn">
		<xsl:choose>
				<xsl:when test="ISBN=''"></xsl:when>
				<xsl:otherwise><br/>ISBN: <xsl:value-of select="ISBN"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
<!--journal articles-->
	<xsl:for-each select=".[@type='article_j']">

	<div class='t2'><p>
	<xsl:value-of select="title"/><xsl:copy-of select="$link"/><br/>
	<xsl:value-of select="authors"/><br/>
	<xsl:value-of select="journal"/>&nbsp;<xsl:value-of select="no"/> (<xsl:value-of select="year"/>), <xsl:value-of select="pages"/>.
	</p></div><br/>
	</xsl:for-each>

<!--collection articles-->
	<xsl:for-each select=".[@type='article_c']" >
	<xsl:variable name="editors">
		<xsl:choose>
			<xsl:when test="edited_by=''"> </xsl:when>
			<xsl:otherwise>, <xsl:value-of select="edited_by"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="series">
		<xsl:choose>
			<xsl:when test="series=''"></xsl:when>
			<xsl:otherwise> (<xsl:value-of select="series"/>
					<xsl:choose>
						<xsl:when test="vol=''"></xsl:when>
						<xsl:otherwise>&nbsp;<xsl:value-of select="vol"/></xsl:otherwise>
					</xsl:choose>)</xsl:otherwise></xsl:choose></xsl:variable>

	<div class='t2'><p>
	<xsl:value-of select="title"/><xsl:copy-of select="$link"/><br/>
	<xsl:value-of select="authors"/><br/>
	<xsl:value-of select="collection"/><xsl:value-of select="$editors"/><xsl:value-of select="$series"/>, <xsl:value-of select="pub_place"/>, <xsl:value-of select="year"/>, <xsl:value-of select="pages"/>.
	<xsl:copy-of select="$isbn"/>
	</p></div><br/>
	</xsl:for-each>
	
	<xsl:for-each select=".[@type='monograph']" >
	
	<xsl:variable name="editors">
		<xsl:choose>
			<xsl:when test="edited_by!=''"><xsl:value-of select="edited_by"/>, </xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="series">
		<xsl:choose>
			<xsl:when test="series=''"></xsl:when>
			<xsl:otherwise><xsl:value-of select="series"/>
					<xsl:choose>
						<xsl:when test="vol=''"></xsl:when>
						<xsl:otherwise>&nbsp;<xsl:value-of select="vol"/></xsl:otherwise>
					</xsl:choose>, </xsl:otherwise></xsl:choose></xsl:variable>

	<div class='t2'><p>
	<xsl:value-of select="title"/><xsl:copy-of select="$link"/><br/>
	<xsl:value-of select="authors"/><br/>
	<xsl:value-of select="$editors"/><xsl:value-of select="$series"/><xsl:value-of select="pub_place"/>, <xsl:value-of select="year"/>.
	<xsl:copy-of select="$isbn"/>
	</p></div><br/>
	</xsl:for-each>

</xsl:when>
</xsl:choose>
</xsl:for-each>
</xsl:for-each>
</body>
</html>
</xsl:template>
</xsl:stylesheet>