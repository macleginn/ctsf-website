var TABLES = {
    'Baima': '<head><meta charset="UTF-8"></head><div><h1>Baima</h1><h2>Consonants</h2><h3>Plain series:</h3><table><tr>    <td></td>    <td>bilabial</td>    <td>dental</td>    <td>alveolar</td>    <td>postalveolar</td>    <td>alveolo-palatal</td>    <td>palatal</td>    <td>velar</td>    <td>glottal</td></tr><tr>    <td>plosive</td>    <td>b, p, pʰ</td>    <td>d, t, tʰ</td>    <td></td>    <td></td>    <td></td>    <td></td>    <td>g, k, kʰ</td>    <td></td></tr><tr>    <td>nasal</td>    <td>m</td>    <td>n</td>    <td></td>    <td></td>    <td></td>    <td>ɲ</td>    <td>ŋ</td>    <td></td></tr><tr>    <td>trill</td>    <td></td>    <td>r</td>    <td></td>    <td></td>    <td></td>    <td></td>    <td></td>    <td></td></tr><tr>    <td>fricative</td>    <td></td>    <td></td>    <td>s, sʰ, z</td>    <td>ʃ, ʃʰ, ʒ</td>    <td>ɕ, ɕʰ, ʑ</td>    <td></td>    <td></td>    <td>ɦ</td></tr><tr>    <td>affricate</td>    <td></td>    <td></td>    <td>dz, ts, tsʰ</td>    <td>dʒ, tʃ, tʃʰ</td>    <td>dʑ, tɕ, tɕʰ</td>    <td></td>    <td></td>    <td></td></tr><tr>    <td>lateral approximant</td>    <td></td>    <td>l</td>    <td></td>    <td></td>    <td></td>    <td></td>    <td></td>    <td></td></tr></table><h3>Pre-nasalised series:</h3><table><tr>    <td></td>    <td>bilabial</td>    <td>dental</td>    <td>alveolar</td>    <td>postalveolar</td>    <td>alveolo-palatal</td>    <td>velar</td></tr><tr>    <td>plosive</td>    <td>ⁿb</td>    <td>ⁿd</td>    <td></td>    <td></td>    <td></td>    <td>ⁿg</td></tr><tr>    <td>affricate</td>    <td></td>    <td></td>    <td>ⁿdz</td>    <td>ⁿdʒ</td>    <td>ⁿdʑ</td>    <td></td></tr></table><h2>Vowels</h2><h3>Plain series:</h3><table><tr>    <td></td>    <td>front</td>    <td>central</td>    <td>back</td></tr><tr>    <td>close</td>    <td>i, y</td>    <td></td>    <td>u</td></tr><tr>    <td>close-mid</td>    <td>e, ø</td>    <td></td>    <td>o</td></tr><tr>    <td>mid</td>    <td></td>    <td>ə</td>    <td></td></tr><tr>    <td>open-mid</td>    <td>ɛ</td>    <td></td>    <td>ɔ</td></tr><tr>    <td>near-open</td>    <td></td>    <td>ɐ</td>    <td></td></tr><tr>    <td>open</td>    <td>a</td>    <td></td>    <td>ɑ</td></tr></table><h3>Nasalised series:</h3><table><tr>    <td></td>    <td>front</td>    <td>back</td></tr><tr>    <td>close-mid</td>    <td></td>    <td>õ</td></tr><tr>    <td>open-mid</td>    <td>ɛ̃</td>    <td></td></tr><tr>    <td>open</td>    <td></td>    <td>ɑ̃</td></tr></table><h3>Diphthongs:</h3><p>iø, ue, iɛ, yɛ, uɛ, io, iɔ, uɔ, iɑ, uɑ, uɑ̃</p></div>',
    'Denjongka': '<div>Denjongka</div>',
    'Dolpo Tibetan': '',
    'Dongwang Tibetan': '',
    'Dzongkha': '',
    'gLo Tibetan': '',
    'gSerpa': '',
    'Humla Bhotia': '',
    'Kami Tibetan': '',
    'Khalong Tibetan': '',
    'Kham Tibetan': '',
    'Kyirong Tibetan': '',
    'Labrang Tibetan': '',
    'Lhasa Tibetan': '',
    'Mugom Tibetan': '',
    'Nyinpa Cone': '',
    'Rgyalthang Tibetan': '',
    'Sherpa': '',
    'Soghpo Tibetan': '',
    'Spiti Tibetan': '',
    'Zhongu Tibetan': ''
}

function makeInfoWindowEvent(map, infowindow, contentString, marker) {
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.setContent(contentString);
    infowindow.open(map, marker);
  });
};

function initialize() {
    var mapOptions = {
        center: new google.maps.LatLng(32.891855, 90.775381),
        zoom: 6
    };
    var map = new google.maps.Map(document.getElementById("canvas"), mapOptions);
    var names = ['Baima', 'Denjongka', 'Dolpo Tibetan', 'Dongwang Tibetan', 'Dzongkha', 'gLo Tibetan', 'gSerpa', 'Humla Bhotia', 'Kami Tibetan', 'Khalong Tibetan', 'Kham Tibetan', 'Kyirong Tibetan', 'Labrang Tibetan', 'Lhasa Tibetan', 'Mugom Tibetan', 'Nyinpa Cone', 'Rgyalthang Tibetan', 'Sherpa', 'Soghpo Tibetan', 'Spiti Tibetan', 'Zhongu Tibetan'];
    var lats = [32.733, 27.311, 29.2144, 28.536776, 27.466667, 29.19537, 31.879, 30.307, 27.928496, 32.10872, 31.47853, 28.854444, 35.204, 29.65, 29.494, 34.45727, 27.825973, 27.855, 30.851, 32.2465, 32.640579];
    var longs = [104.318, 88.469, 83.119, 99.659034, 89.641667, 83.94885, 100.722, 81.649, 101.280212, 100.98728, 99.95257, 85.296667, 102.52, 91.116667, 82.097, 103.08387, 99.707128, 86.788, 101.926, 78.03707, 103.597121];
    var markers = []
    for (var i = 0; i < names.length; i++) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lats[i], longs[i]),
            title: names[i],
            map: map
        });
        var iw = new google.maps.InfoWindow();
        makeInfoWindowEvent(map, iw, TABLES[names[i]], marker)
    };
};